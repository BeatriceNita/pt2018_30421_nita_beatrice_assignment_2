package Shop;

public class Client {

	private int arrivalTime;
	private int serviceTime;
	
    public Client(int arrivalTime, int serviceTime ) {
		this.arrivalTime = arrivalTime;
	    this.serviceTime = serviceTime;
	    }
	
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public  int getArrivalTime() {
		return arrivalTime;
	}
	
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
	
	public int getServiceTime() {
		return serviceTime;
	}

	public String toString() {
		return String.format("Arrival Time: %d / Service Time: %d ", arrivalTime, serviceTime);
	}
}
