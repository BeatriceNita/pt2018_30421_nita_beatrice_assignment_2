package Shop;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Gui implements ActionListener {
	
	private JFrame frame = new JFrame("Queue processing");
	private JPanel input = new JPanel();
	private JPanel start = new JPanel();

	private JLabel minServTimeLabel = new JLabel("Min service time:");
	private JLabel maxServTimeLabel = new JLabel("Max service time:");
	private JLabel finishTimeLabel = new JLabel("Finish time:");
	private JLabel nrOfQueuesLabel = new JLabel("Number of queues:");

	private JTextField minServTimeText = new JTextField();
	private JTextField maxServTimeText = new JTextField();
	private JTextField finishTimeText = new JTextField();
	private JTextField nrOfQueuesText = new JTextField();

	//private int nrOfClients;
	private int minServTime;
	private int maxServTime;
	private int finishTime;
	private int nrOfQueues;

	private JButton startButton = new JButton("Start");

	private ClientGenerator clientGenerator;
	private ThreadsGui threadsGui;
	private LogOfEvents loe;

	public  Gui() {

		frame.setSize(500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		input.setLayout(new GridLayout(4, 2));

		input.add(minServTimeLabel);
		input.add(minServTimeText);
		input.add(maxServTimeLabel);
		input.add(maxServTimeText);
		input.add(finishTimeLabel);
		input.add(finishTimeText);
		input.add(nrOfQueuesLabel);
		input.add(nrOfQueuesText);

		minServTimeText.addActionListener(this);
		maxServTimeText.addActionListener(this);
		finishTimeText.addActionListener(this);
		nrOfQueuesText.addActionListener(this);

		startButton.setActionCommand("start");
		startButton.addActionListener(this);
		start.setLayout(new FlowLayout());
		start.add(startButton);

		frame.add(input, BorderLayout.CENTER);
		frame.add(start, BorderLayout.SOUTH);
	}

	@Override
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("start")) {
			minServTime = Integer.parseInt(minServTimeText.getText());
			maxServTime = Integer.parseInt(maxServTimeText.getText());
			finishTime = Integer.parseInt(finishTimeText.getText());
			nrOfQueues = Integer.parseInt(nrOfQueuesText.getText());
			
			loe = new LogOfEvents();
			clientGenerator = new ClientGenerator(minServTime, maxServTime, finishTime, nrOfQueues, loe);
			Thread clientGenThread = new Thread(clientGenerator);
			clientGenThread.start();
		}
	}
}