package Shop;

import java.util.ArrayList;
import java.util.Random;

public class ClientGenerator implements Runnable {

	private int minServiceTime, maxServiceTime, finishTime;
	private int nrOfQueues;

	private Scheduler scheduler;
	private LogOfEvents loe;
	private ThreadsGui threadsGui;
	
	public ClientGenerator(int minServiceTime, int maxServiceTime, int finishTime, int nrOfQueues, LogOfEvents loe) {
		this.maxServiceTime = maxServiceTime;
		this.minServiceTime = minServiceTime;
		this.finishTime = finishTime;
		this.nrOfQueues = nrOfQueues;
		this.loe = loe;
		
		scheduler = new Scheduler();

		for (int i = 0; i < nrOfQueues; i++) {
			Queue queue = new Queue(i, loe);
			scheduler.addQueue(queue);
		}

		threadsGui = new ThreadsGui(nrOfQueues, scheduler, this);
	}

	@Override
	public void run() 
	{
      int currentTime = 0;

     			while (currentTime < finishTime) {
     				currentTime++;
     				int processTime = (int) (Math.random() * (maxServiceTime - minServiceTime) * 2);
     				int arrivalTime = (int) (currentTime + Math.random() * 3);
     				
     				Client client = new Client(arrivalTime, processTime);
     				scheduler.sendClientToQueue(client, arrivalTime);
     				scheduler.serveClients();
     				threadsGui.displayData(scheduler.getQueues());

     				try {
     					Thread.sleep(500);
     				} catch (InterruptedException e) {
     					e.printStackTrace();
     				}
     			}
     			loe.appendText("Stopped");
     			scheduler.stopQueues();
    }
	
	/*public Queue getMin(){
		int min = queues.get(0).getClients().size();
		minQueue = queues.get(0);
        for (Queue queue : queues) {
            if (queue.getClients().size() < min) 
            {
                min = queue.getClients().size();
                minQueue=queue;
            }
        }
        return minQueue;
	}
	
	public void generateQueueProgress() {
		String s = " ";
		for(int i = 0; i < nrOfQueues; i++) {
			for(Client c : clients) {
			    s += c.toString();
			}
		}
	}
	
	public  int getNrOfClients() {
		int maxClients = 0;

		for (int i = 0; i < nrOfQueues; i++) {
				maxClients += queues.get(i).getQueueSize();
		}
		return maxClients;
	}*/
	
}
