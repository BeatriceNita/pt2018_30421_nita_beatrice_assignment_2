package Shop;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class LogOfEvents {

	private JFrame frame = new JFrame("Events");
	private JPanel panel = new JPanel();
	
	private JTextArea textArea = new JTextArea();
	
    public LogOfEvents() {
    	
    	frame.setSize(700, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		panel.setLayout(new GridLayout(1, 1));
		
		textArea.setEditable(true);
		panel.add(textArea);
		
		frame.add(panel, BorderLayout.CENTER);
    }
    
    public void appendText(String string) {
    	textArea.append(string);
    }
	
}
