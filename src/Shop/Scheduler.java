package Shop;

import java.util.ArrayList;

public class Scheduler{
	
	private ArrayList<Queue> queues;

	int[] countClients = new int[100];

	public  Scheduler() {
		queues = new ArrayList<Queue>();
	}

	public  void sendClientToQueue(Client c, int arrivalTime) {
		countClients[arrivalTime]++;

		int minNrOfClients = 9999;
		Queue queue = null;
		for (int i = 0; i < queues.size(); i++) {
			if (!queues.get(i).isClosed() && queues.get(i).getQueueSize() < minNrOfClients) {
				minNrOfClients = queues.get(i).getQueueSize();
				queue = queues.get(i);
			}
		}
		queue.addClient(c);
	}

	public  void addQueue(Queue q) {
		queues.add(q);
	}

	public  void serveClients() {
		for (Queue q : queues) {
			Thread queueThread = new Thread(q);
			queueThread.start();
		}
	}

	public  void stopQueues() {
		for (Queue q : queues) {
			q.setRunning(false);
		}
	}

	public  ArrayList<Queue> getQueues() {
		return queues;
	}

}