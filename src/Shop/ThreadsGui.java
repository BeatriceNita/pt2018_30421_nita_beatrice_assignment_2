package Shop;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.*;

import Shop.Queue;

public class ThreadsGui {

	private JFrame frame = new JFrame("Shop");
	
	private JPanel shopPanel = new JPanel();
	private JPanel queuesPanel = new JPanel();
	private JPanel infoPanel = new JPanel();
	private JPanel openPanel = new JPanel();

	private int nrOfQueues;
	private ArrayList <Queue> queues;
	private Scheduler scheduler;
	private ClientGenerator clientGenerator;
	
	public  ThreadsGui(int nrOfQueues, Scheduler scheduler, ClientGenerator clientGenerator) {
		this.nrOfQueues = nrOfQueues;
		this.scheduler = scheduler;
		this.clientGenerator = clientGenerator;
		
		queues = new ArrayList <Queue> ();
		
		frame.setSize(800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);
		
		infoPanel.setLayout(new GridLayout(nrOfQueues, 1));
		queuesPanel.setLayout(new GridLayout(nrOfQueues, 1));
		shopPanel.setLayout(new GridLayout(1, 3));

		for (int i = 0; i < nrOfQueues; i++) {
			JLabel label = new JLabel(" Queue " + i);
			infoPanel.add(label);
		}

		shopPanel.add(infoPanel);
		shopPanel.add(queuesPanel);
		frame.add(shopPanel);
	}
	
	public  void displayData(ArrayList<Queue> queues) {
		this.queues = queues;
		queuesPanel.removeAll();
		queuesPanel.revalidate();

		for (int i = 0; i < queues.size(); i++) {
			JList<Client> clients = new JList<Client>(queues.get(i).getClientsArray());
			JScrollPane clientsScroll = new JScrollPane(clients);

			queuesPanel.add(clientsScroll);
		}

		queuesPanel.repaint();
		queuesPanel.revalidate();

	}

}