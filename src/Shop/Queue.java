package Shop;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

//import Shop.Client;

public class Queue implements Runnable {

	private int qID;
	
	private BlockingQueue <Client> clients;
	private boolean isRunning = true;
	private boolean isClosed = false;
	
	private AtomicInteger clientsServed = new AtomicInteger();
	private AtomicInteger serviceTime = new AtomicInteger();
	
	private LogOfEvents loe;
	private Object lock = new Object();
	
	public Queue(int qID, LogOfEvents loe){
		  this.qID = qID;
		  this.loe = loe;
		  clients = new LinkedBlockingQueue<Client>();
	}
	
	public int getQueueID() {
		return qID;
	}
	 
	@Override
	public void run() 
	{
		synchronized(lock) 
	  	{
	  		while (isRunning)
	  		{
	  				try 
	  				{
	  					while (clients.size() > 0) {
							for (Client client : clients) {
								Thread.sleep(client.getServiceTime() * 500);
	  							clients.remove(client);
	  							}
							}
					}
	  				catch (InterruptedException e) 
	  				{
	  					e.printStackTrace();
	  				}
	  			}
	  		}

	  	}
	  		
	public  boolean isRunning() {
	  	return isRunning;
	}

	public  void setRunning(boolean isRunning) {
	  	this.isRunning = isRunning;
	}
	
	public void addClient(Client client){
		loe.appendText("Queue no " +qID+ " -> arrival time: " +client.getArrivalTime()+ ", service time: " +client.getServiceTime() +"\n");
		serviceTime.addAndGet(client.getServiceTime());
		clientsServed.getAndIncrement();
		clients.add(client);
	}
	
	public  Client[] getClientsArray() {
		Client[] clientsArray = new Client[clients.size()];
		clients.toArray(clientsArray);

		return clientsArray;
	}
	
	public BlockingQueue<Client> getClients(){
		return clients;
	}
	
	public  int getQueueSize() {
		return clients.size();
	}
	
	public  void setClosed(boolean b) {
		isClosed = b;
	}

	public  boolean isClosed() {
		return isClosed;
	}
}